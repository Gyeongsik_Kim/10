-- phpMyAdmin SQL Dump
-- version 4.2.12deb2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 16-03-27 03:53
-- 서버 버전: 5.5.44-0+deb8u1
-- PHP 버전: 5.6.17-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 데이터베이스: `appjam`
--

-- --------------------------------------------------------

--
-- 테이블 구조 `group`
--

CREATE TABLE IF NOT EXISTS `group` (
`no` bigint(255) NOT NULL,
  `groupId` text COLLATE utf8mb4_bin NOT NULL,
  `startDate` int(8) NOT NULL,
  `endDate` int(8) NOT NULL,
  `name` varchar(20) COLLATE utf8mb4_bin NOT NULL,
  `max` int(255) NOT NULL,
  `xy` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- 테이블의 덤프 데이터 `group`
--

INSERT INTO `group` (`no`, `groupId`, `startDate`, `endDate`, `name`, `max`, `xy`) VALUES
(1, 'Mzk0ODc4MjQ2NQ', 20160326, 20160326, '테스트', 123, 'aa'),
(2, 'ODE0NTMxNDczNg', 20160326, 20160326, '테스트', 123, 'aa'),
(3, 'NjM1NjkzMzg5OQ', 20160326, 20160326, '테스트', 123, 'aa'),
(4, 'OTcxNzQzNTI4Ng', 20160326, 20160326, '테스트', 123, 'aa'),
(5, 'OTE3NjQ3NTYx', 20160326, 0, '테스트', 123, 'aa'),
(6, 'NzYzNTM3MDU5Mg', 20160326, 0, '테스트', 123, 'aa'),
(7, 'MTY0NzUyNDU', 2016326, 0, '테스트', 123, 'aa'),
(8, 'NzY0NTg5NTg5NA', 20160326, 0, '테스트', 123, 'aa'),
(9, 'MjAyMjc3ODYxOQ', 201621, 0, 'aaaaa', 5, '123213'),
(10, 'NDMzNjU3NzQzNA', 2016227, 0, 'gg', 5, '123213'),
(11, 'Mjc3NjUyMTY2Mw', 2016227, 0, 'gg', 5, '123213'),
(12, 'NzczNTQ5Mjk1Ng', 2016227, 0, 'rt', 22, '123213'),
(13, 'Mzg5MTIyOTA4OQ', 2016227, 0, 'rt', 22, '123213'),
(14, 'MTY5NjQ5MDcxMA', 2016227, 0, 'rt', 22, '123213'),
(15, 'NzYwMzExMjgzMQ', 2016227, 0, 'rt', 22, '123213'),
(16, 'NzI4NTgzNjE1', 2016227, 0, 'rt', 22, '123213'),
(17, 'NzI3OTMxOTc0Mg', 201621, 0, 'aa', 0, '123213'),
(18, 'NzIxMTgzNjYyMw', 2016231, 0, '5', 0, '123213');

-- --------------------------------------------------------

--
-- 테이블 구조 `people`
--

CREATE TABLE IF NOT EXISTS `people` (
`no` bigint(255) NOT NULL,
  `id` text COLLATE utf8mb4_bin NOT NULL,
  `ip` varchar(16) COLLATE utf8mb4_bin NOT NULL,
  `time` varchar(15) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- 덤프된 테이블의 인덱스
--

--
-- 테이블의 인덱스 `group`
--
ALTER TABLE `group`
 ADD PRIMARY KEY (`no`);

--
-- 테이블의 인덱스 `people`
--
ALTER TABLE `people`
 ADD UNIQUE KEY `no` (`no`);

--
-- 덤프된 테이블의 AUTO_INCREMENT
--

--
-- 테이블의 AUTO_INCREMENT `group`
--
ALTER TABLE `group`
MODIFY `no` bigint(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- 테이블의 AUTO_INCREMENT `people`
--
ALTER TABLE `people`
MODIFY `no` bigint(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
